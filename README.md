epic-git
========

Templates, SmartPhrases, and links to outside resources for Epic EMR

[Note Templates](note_templates)
--------------------------------

`*.md` for easy download, copy, and paste, with some elemental layout.
`*.docx` - so far original formatting is only preserved by copying into MS Word.
`*.odt` - open format, should preserve most formatting. (MS Word export)

External Resources - Templates and SmartPhrases
-----------------------------------------------

Some links that I found online that may or may not contain useful templates
and SmartPhrases.

Note that those at times refer to institution-specific SmartPhrases.

* https://inba.info/epic-smart-phrases_5751525fb6d87f8c1e8b5783.html
* https://imweb.swmed.edu/imweb/images/stories/hopsitals/phhs/phhs-epic-smartphrases.pdf
