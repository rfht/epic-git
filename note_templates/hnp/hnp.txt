History and Physical
====================

Name: @NAME@  Age: @AGE@
DOB: @DOB@  MRN: @MRN@  PCP: @PCP@
Admit Date: @ADMITDT@
 
Patient seen on @TODAYDATE@.***
 
Chief Complaint:
----------------

@NAME@, a @AGE@ @SEX@ has been admitted with a chief complaint of ***
 
Subjective:
===========
 
Source: Information obtained from ***
 
History of Present Illness: 
---------------------------
 
***
 
Home Oxygen/CPAP: ***
Home Assistive Devices: ***
 
@ROSBYAGE@
 
@MEDICALHX@
 
@SURGICALHX@
 
@FAMHX@
 
@SOCH@
 
@ALLERGY@
 
@PTAMEDSREFRESH@
@MEDCOMMENTS@
 
Objective:
==========
 
Most Recent Vital Signs:
------------------------

@IPVITALS(36)@
 
Min/Max Temp past 24 hours:@FLOWSTAT(6:24)@
 
I&O:
----

@IOBRIEF@
 
PHYSICAL EXAMINATION: 
---------------------
 
***
@PHYSEXAM@
 
Labs/Imaging:
=============
 
@RESULTRCNT(36h)@
 
Imaging:
--------

@LASTIMGRESULTS@
 
CARDIOLOGICAL TESTING/MONITORING:
---------------------------------
 
@EKGIP@
 
Assessment and Plan:
====================

ASSESSMENT:
-----------

@AGE@ @SEX@ with ***

Patient status is ***

PLAN:
-----

@PROBLEMS@

***

FEN: 
----

Fluids: ***
Electrolytes: ***
@DIETORD@ ***

Prophylaxis: ***
Code Status: @RRCODESTATUS@
Surrogate Decision Maker: ***
Disposition: ***
Anticipated Discharge Date: ***

@ESIGNATURE@
